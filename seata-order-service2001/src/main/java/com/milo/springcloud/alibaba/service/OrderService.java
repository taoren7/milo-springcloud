package com.milo.springcloud.alibaba.service;


import com.milo.springcloud.alibaba.domain.Order;

/**
 * @Author Milo
 * @Description
 * @Date 2020-05-15 16:38
 **/
public interface OrderService
{
    void create(Order order);
}
