package com.milo.springcloud.alibaba.service;


import com.milo.springcloud.entities.CommonResult;
import com.milo.springcloud.entities.Payment;
import org.springframework.stereotype.Component;

/**
 * @Author Milo
 * @Description
 * @Date 2020-05-15 13:27
 **/
@Component
public class PaymentFallbackService implements PaymentService
{
    @Override
    public CommonResult<Payment> paymentSQL(Long id)
    {
        return new CommonResult<>(44444,"服务降级返回,---PaymentFallbackService",new Payment(id,"errorSerial"));
    }
}
