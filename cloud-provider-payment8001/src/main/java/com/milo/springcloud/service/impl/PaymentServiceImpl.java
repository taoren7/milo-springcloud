package com.milo.springcloud.service.impl;

import com.milo.springcloud.dao.PaymentDao;
import com.milo.springcloud.entities.Payment;
import com.milo.springcloud.service.PaymentService;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @Author Milo
 * @Description
 * @Date 2020-05-10 22:59
 **/
@Service
public class PaymentServiceImpl implements PaymentService {

    @Resource
    private PaymentDao paymentDao;

    public int create(Payment payment){
        return paymentDao.create(payment);
    }

    public Payment getPaymentById(Long id){
        return paymentDao.getPaymentById(id);
    }
}
