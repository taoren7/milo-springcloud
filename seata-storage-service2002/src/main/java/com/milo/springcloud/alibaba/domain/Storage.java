package com.milo.springcloud.alibaba.domain;

import lombok.Data;
/**
 * @Author Milo
 * @Description
 * @Date 2020-05-15 17:06
 **/
@Data
public class Storage {

    private Long id;

    /**
     * 产品id
     */
    private Long productId;

    /**
     * 总库存
     */
    private Integer total;

    /**
     * 已用库存
     */
    private Integer used;

    /**
     * 剩余库存
     */
    private Integer residue;
}
