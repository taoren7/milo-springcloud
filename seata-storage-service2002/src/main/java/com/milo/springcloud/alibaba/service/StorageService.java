package com.milo.springcloud.alibaba.service;

/**
 * @Author Milo
 * @Description
 * @Date 2020-05-15 17:06
 **/
public interface StorageService {
    /**
     * 扣减库存
     */
    void decrease(Long productId, Integer count);
}
